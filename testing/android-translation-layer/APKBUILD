# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=android-translation-layer
pkgver=0_git20240428
pkgrel=0
_commit="a99dfd80ccff8b39ca7014d42c93586dbe6e7edf"
pkgdesc="A translation layer for running Android apps on a Linux system"
url="https://gitlab.com/android_translation_layer/android_translation_layer"
arch="x86_64 aarch64 armv7"
license="GPL-3.0-only"
# libopensles-standalone is not strictly required but some Android applications depend on it
depends="libopensles-standalone"
makedepends="
	alsa-lib-dev
	bionic_translation-dev
	art_standalone-dev
	ffmpeg-dev
	glib-dev
	gtk4.0-dev
	java-common
	libgudev-dev
	libportal-dev
	meson
	openjdk8-jdk
	openxr-dev
	skia-sharp-dev
	vulkan-loader-dev
	"
subpackages="$pkgname-dbg"
source="https://gitlab.com/mis012/android_translation_layer/-/archive/$_commit/android_translation_layer-$_commit.tar.gz"
builddir="$srcdir/android_translation_layer-$_commit"

build() {
	abuild-meson \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
bef47fb541b61532d84f24a80743aff1d23ea9de495118f2695ceeb7794ad76cd752e379161073ad42b220eff13ef404017dae712714a19ad6b5d7b636f67fae  android_translation_layer-a99dfd80ccff8b39ca7014d42c93586dbe6e7edf.tar.gz
"
