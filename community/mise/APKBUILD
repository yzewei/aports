# Maintainer: Jeff Dickey <alpine@mise.jdx.dev>
pkgname=mise
pkgver=2024.5.21
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://mise.jdx.dev"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
provides="rtx=$pkgver-r$pkgrel"
replaces="rtx"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdx/mise/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin mise
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mise -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
817f59ba0f3db90cd6b985bed0b40452312da603c313e76815b2aab2325dd32d0cc36122cc7916dad6579e683c7317d14f98734707a954c1f8178f535dcb171f  mise-2024.5.21.tar.gz
"
